-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2020 at 02:52 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shoesmart`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_menus`
--

CREATE TABLE `admin_menus` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `url` varchar(250) NOT NULL,
  `scheme` varchar(10) NOT NULL,
  `host` varchar(50) NOT NULL,
  `path` varchar(100) NOT NULL,
  `qs` varchar(150) NOT NULL DEFAULT '',
  `child` int(11) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_menus`
--

INSERT INTO `admin_menus` (`id`, `title`, `icon`, `url`, `scheme`, `host`, `path`, `qs`, `child`, `status`, `parent_id`, `created`, `updated`) VALUES
(1, 'Dashboard', 'fa-dashboard', 'administrator', '', '', '', '', 0, 1, 1, '2018-09-15 23:21:15', 1602238586),
(3, 'Product', 'fas fa-rocket', '#', '', '', '', '', 6, 1, 2, '2018-09-16 00:46:53', 1602312711),
(4, 'Website Management', 'fas fa-database', '#', '', '', '', '', 5, 1, 2, '2020-04-08 04:05:07', 1602325817),
(5, 'Orders', 'fas fa-shopping-cart', '#', '', '', '', '', 2, 1, 2, '2020-10-10 07:07:44', 1602313754),
(6, 'Settings', 'fas fa-cog', '#', '', '', '', '', 2, 1, 2, '2020-04-08 04:05:17', 1598082031),
(7, 'Users', 'fa-user', '#', '', '', '', '', 2, 1, 2, '2020-04-08 04:24:56', 1598169137),
(9, 'Photos', 'fa-camera-retro', 'creator/manage-content/photos', '', '', '', '', 0, 0, 3, '2020-04-08 04:51:11', 1598324565),
(10, 'Videos', 'ti-video-clapper', 'creator/manage-content/videos', '', '', '', '', 0, 0, 3, '2020-04-08 04:51:11', 1598324641),
(11, 'Categories', 'fa-tags', 'categories', '', '', '', '', 0, 1, 4, '2020-04-08 04:57:58', 1602350254),
(12, 'Admin Menu', 'fa-th-large', 'admin/menu', '', '', '', '', 0, 1, 6, '2020-04-08 05:09:26', 0),
(13, 'Admin Users', 'ti-key', 'admin/users', '', '', '', '', 0, 1, 7, '2020-04-08 05:09:26', 0),
(14, 'Admin Groups', 'ti-layers-alt', 'admin/users/groups', '', '', '', '', 0, 1, 7, '2020-04-08 05:09:26', 0),
(23, 'Permission', 'fas fa-user-shield', 'admin/menu/permission', '', '', '', '', 0, 1, 6, '2020-08-23 16:01:19', 0),
(24, 'List Products', 'fas fa-archive', 'administrator/products', '', '', '', '', 0, 1, 3, '2020-08-24 06:42:07', 1602312740),
(25, 'Catalog', 'fas fa-bars', 'administrator/catalog', '', '', '', '', 0, 1, 3, '2020-10-10 06:57:54', 0),
(26, 'Product Detail', 'fas fa-search-plus', 'administrator/detail', '', '', '', '', 0, 1, 3, '2020-10-10 06:59:35', 0),
(28, 'Top Menu', 'fas fa-bars', 'admin/menu/top-menu', '', '', '', '', 0, 1, 4, '2020-10-10 10:25:22', 0),
(29, 'Main Menu', 'fas fa-bars', 'admin/menu/main-menu', '', '', '', '', 0, 1, 4, '2020-10-10 10:25:57', 0),
(30, 'Footer Menu', 'fas fa-bars', 'admin/menu/footer-menu', '', '', '', '', 0, 1, 4, '2020-10-10 10:28:04', 0),
(31, 'Style', 'fab fa-shirtsinbulk', 'categories/style', '', '', '', '', 0, 1, 4, '2020-10-11 07:14:04', 0),
(32, 'size', 'fas fa-search-plus', 'administrator/size', '', '', '', '', 0, 1, 3, '2020-10-11 14:24:09', 0),
(33, 'color', 'fas fa-brush', 'administrator/color', '', '', '', '', 0, 1, 3, '2020-10-11 14:24:49', 0),
(34, 'cart', 'fas fa-luggage-cart', 'order/cart', '', '', '', '', 0, 1, 5, '2020-10-11 16:49:23', 0),
(35, 'wishlist', 'fas fa-heart', 'order/wishlist', '', '', '', '', 0, 1, 5, '2020-10-11 16:50:27', 2020),
(36, 'rate', 'fas fa-star', 'administrator/rate', '', '', '', '', 0, 1, 3, '2020-10-11 17:07:34', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `product_id` varchar(250) NOT NULL,
  `type` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `expired_date` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'men', 1, '2020-10-11 14:08:54', '0000-00-00 00:00:00'),
(2, 'woman', 1, '2020-10-11 14:10:26', '0000-00-00 00:00:00'),
(3, 'kids', 1, '2020-10-11 14:12:28', '2020-10-11 07:12:28');

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE `color` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `hex` varchar(250) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`id`, `name`, `hex`, `status`, `created_at`, `updated_at`) VALUES
(1, 'red', '#ff0000', 1, '2020-10-11 21:59:24', '2020-10-11 19:59:24'),
(2, 'green', '#00ff06', 1, '2020-10-11 22:00:16', '2020-10-11 20:00:16'),
(3, 'blue', '#1000ff', 1, '2020-10-11 22:00:59', '2020-10-11 20:00:59'),
(4, 'pink', '#ff009c', 1, '2020-10-11 22:01:44', '2020-10-11 20:01:44'),
(5, 'violet', '#9a00ff', 1, '2020-10-11 22:02:13', '2020-10-11 20:02:13');

-- --------------------------------------------------------

--
-- Table structure for table `footer_menu`
--

CREATE TABLE `footer_menu` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url` longtext NOT NULL,
  `parent_id` int(11) NOT NULL,
  `child` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer_menu`
--

INSERT INTO `footer_menu` (`id`, `title`, `url`, `parent_id`, `child`, `status`, `created_at`, `updated_at`) VALUES
(1, 'my account', '#', 0, 3, 1, '2020-10-11 00:41:15', '2020-10-10 17:41:15'),
(2, 'service', '#', 0, 2, 1, '2020-10-11 00:44:36', '2020-10-10 17:44:36'),
(3, 'my account', 'customer/account', 1, 0, 1, '2020-10-11 00:46:46', '2020-10-10 17:46:46'),
(4, 'my wishlist', 'orders/wishlist', 1, 0, 1, '2020-10-11 00:47:36', '0000-00-00 00:00:00'),
(5, 'checkout', 'orders/checkout', 1, 0, 1, '2020-10-11 00:48:21', '2020-10-10 17:48:21'),
(6, 'about us', 'homeshop/about', 2, 0, 1, '2020-10-11 00:51:54', '2020-10-10 17:51:54'),
(7, 'contact us', 'homeshop/contact', 2, 0, 1, '2020-10-11 00:52:28', '2020-10-10 17:52:28');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `log_user`
--

CREATE TABLE `log_user` (
  `id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `edited_content` text NOT NULL,
  `ip_address` varchar(200) NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_on` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `main_menu`
--

CREATE TABLE `main_menu` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url` longtext NOT NULL,
  `parent_id` int(11) NOT NULL,
  `child` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_menu`
--

INSERT INTO `main_menu` (`id`, `title`, `url`, `parent_id`, `child`, `status`, `created_at`, `updated_at`) VALUES
(1, 'home', 'homeshop', 0, 0, 1, '2020-10-10 23:17:14', '0000-00-00 00:00:00'),
(2, 'men', '#', 0, 6, 1, '2020-10-10 23:21:49', '2020-10-10 16:21:49'),
(3, 'woman', '#', 0, 7, 1, '2020-10-10 23:23:10', '2020-10-10 16:23:10'),
(4, 'kids', '#', 0, 7, 1, '2020-10-10 23:25:02', '2020-10-10 16:25:02'),
(5, 'new launching', 'homeshop/new', 0, 0, 1, '2020-10-10 23:31:08', '0000-00-00 00:00:00'),
(6, 'promo', 'homeshop/promo', 0, 0, 1, '2020-10-10 23:32:11', '0000-00-00 00:00:00'),
(7, 'catalog', 'homeshop/catalog', 0, 0, 1, '2020-10-10 23:45:29', '2020-10-10 16:45:29'),
(8, 'casual', 'homeshop/category/men/casual', 2, 0, 1, '2020-10-10 23:52:22', '2020-10-10 16:52:22'),
(9, 'jacket', 'homeshop/category/men/jacket', 2, 0, 1, '2020-10-10 23:53:22', '2020-10-10 16:53:22'),
(10, 'shirt', 'homeshop/category/men/shirt', 2, 0, 1, '2020-10-10 23:54:36', '2020-10-10 16:54:36'),
(11, 'foot', 'homeshop/category/men/foot', 2, 0, 1, '2020-10-10 23:55:59', '2020-10-10 16:55:59'),
(12, 'casual', 'homeshop/category/woman/casual', 3, 0, 1, '2020-10-10 23:56:31', '2020-10-10 16:56:31'),
(13, 'shirt', 'homeshop/category/woman/shirt', 3, 0, 1, '2020-10-10 23:57:04', '2020-10-10 16:57:04'),
(14, 'dress', 'homeshop/category/woman/dress', 3, 0, 1, '2020-10-10 23:57:43', '2020-10-10 16:57:43'),
(15, 'pants', 'homeshop/category/woman/pants', 3, 0, 1, '2020-10-10 23:58:25', '2020-10-10 16:58:25'),
(16, 'skirt', 'homeshop/category/woman/skirt', 3, 0, 1, '2020-10-10 23:59:04', '2020-10-10 16:59:04'),
(17, 'underwear', 'homeshop/category/woman/underwear', 3, 0, 1, '2020-10-11 00:03:06', '2020-10-10 17:03:06'),
(18, 'underwear', 'homeshop/category/men/underwear', 2, 0, 1, '2020-10-11 00:03:40', '2020-10-10 17:03:40'),
(19, 'casual', 'homeshop/category/kids/casual', 4, 0, 1, '2020-10-11 00:04:22', '2020-10-10 17:04:22'),
(20, 'shirt', 'homeshop/category/kids/shirt', 4, 0, 1, '2020-10-11 00:04:45', '2020-10-10 17:04:45'),
(21, 'school', 'homeshop/category/kids/school', 4, 0, 1, '2020-10-11 00:05:33', '2020-10-10 17:05:33'),
(22, 'foot', 'homeshop/category/kids/foot', 4, 0, 1, '2020-10-11 00:05:56', '2020-10-10 17:05:56'),
(23, 'skirt', 'homeshop/category/kids/skirt', 4, 0, 1, '2020-10-11 00:06:19', '2020-10-10 17:06:19'),
(24, 'dress', 'homeshop/category/kids/dress', 4, 0, 1, '2020-10-11 00:07:16', '2020-10-10 17:07:16'),
(25, 'suit', 'homeshop/category/men/suit', 2, 0, 1, '2020-10-11 00:07:35', '2020-10-10 17:07:35'),
(26, 'suit', 'homeshop/category/woman/suit', 3, 0, 1, '2020-10-11 00:07:55', '2020-10-10 17:07:55'),
(27, 'suit', 'homeshop/category/kids/suit', 4, 0, 1, '2020-10-11 00:08:08', '2020-10-10 17:08:08');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `product_id` varchar(250) NOT NULL,
  `category` int(11) NOT NULL,
  `style` int(11) NOT NULL,
  `size` varchar(250) NOT NULL,
  `color` varchar(250) NOT NULL,
  `single_price` int(11) NOT NULL,
  `bundle_price` int(11) NOT NULL,
  `promo_code` int(11) DEFAULT NULL,
  `promo_stock` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `picture` longtext NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `id` int(11) NOT NULL,
  `product_id` varchar(250) NOT NULL,
  `rating` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `size`
--

CREATE TABLE `size` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `number` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `size`
--

INSERT INTO `size` (`id`, `name`, `number`, `status`, `created_at`, `updated_at`) VALUES
(1, 's', 15, 1, '2020-10-11 21:31:53', '2020-10-11 19:31:53'),
(2, 'm', 20, 1, '2020-10-11 21:37:00', '2020-10-11 19:37:00'),
(3, 'xl', 30, 1, '2020-10-11 21:37:33', '2020-10-11 19:37:33');

-- --------------------------------------------------------

--
-- Table structure for table `style`
--

CREATE TABLE `style` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `category_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `style`
--

INSERT INTO `style` (`id`, `name`, `category_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'casual', 1, 1, '2020-10-11 14:29:04', '2020-10-11 07:29:04'),
(2, 'jacket', 1, 1, '2020-10-11 15:29:57', '2020-10-11 08:29:57'),
(3, 'shirt', 1, 1, '2020-10-11 15:30:14', '2020-10-11 08:30:14'),
(4, 'foot', 1, 1, '2020-10-11 15:32:40', '2020-10-11 08:32:40'),
(5, 'casual', 2, 1, '2020-10-11 16:16:40', '2020-10-11 09:16:40'),
(6, 'shirt', 2, 1, '2020-10-11 16:16:50', '2020-10-11 09:16:50'),
(7, 'dress', 2, 1, '2020-10-11 16:17:11', '2020-10-11 09:17:11'),
(8, 'pants', 2, 1, '2020-10-11 16:17:23', '2020-10-11 09:17:23'),
(9, 'skirt', 2, 1, '2020-10-11 16:17:44', '2020-10-11 09:17:44'),
(10, 'underwear', 2, 1, '2020-10-11 16:19:24', '2020-10-11 09:19:24'),
(11, 'underwear', 1, 1, '2020-10-11 17:03:09', '2020-10-11 10:03:09'),
(12, 'casual', 3, 1, '2020-10-11 17:04:20', '2020-10-11 10:04:20'),
(13, 'shirt', 3, 1, '2020-10-11 17:07:29', '2020-10-11 10:07:29'),
(14, 'school', 3, 1, '2020-10-11 17:11:43', '2020-10-11 10:11:43'),
(15, 'foot', 3, 1, '2020-10-11 17:20:37', '2020-10-11 10:20:37'),
(16, 'skirt', 3, 1, '2020-10-11 17:21:11', '2020-10-11 10:21:11'),
(17, 'dress', 3, 1, '2020-10-11 17:21:35', '2020-10-11 10:21:35'),
(18, 'suit', 1, 1, '2020-10-11 17:21:49', '2020-10-11 10:21:49'),
(19, 'suit', 2, 1, '2020-10-11 17:21:59', '2020-10-11 10:21:59'),
(20, 'suit', 3, 1, '2020-10-11 17:22:10', '2020-10-11 10:22:10');

-- --------------------------------------------------------

--
-- Table structure for table `top_menu`
--

CREATE TABLE `top_menu` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url` longtext NOT NULL,
  `parent_id` int(11) NOT NULL,
  `child` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `top_menu`
--

INSERT INTO `top_menu` (`id`, `title`, `url`, `parent_id`, `child`, `status`, `created_at`, `updated_at`) VALUES
(1, 'store', '/', 0, 0, 1, '2020-10-10 20:29:18', '0000-00-00 00:00:00'),
(2, 'newsletter', '/', 0, 0, 1, '2020-10-10 20:30:10', '0000-00-00 00:00:00'),
(3, 'contact us', 'homeshop/contact', 0, 0, 1, '2020-10-10 20:30:59', '0000-00-00 00:00:00'),
(4, 'lang', '#', 0, 2, 1, '2020-10-10 20:31:53', '2020-10-10 13:31:53'),
(5, 'money', '#', 0, 2, 1, '2020-10-10 20:31:54', '2020-10-10 13:31:54'),
(6, 'eng', '#', 4, 0, 1, '2020-10-10 20:39:20', '2020-10-10 13:39:20'),
(7, 'id', '#', 4, 0, 1, '2020-10-10 20:40:36', '2020-10-10 13:40:36'),
(8, 'USD', '#', 5, 0, 1, '2020-10-10 20:41:37', '0000-00-00 00:00:00'),
(9, 'Rp', '#', 5, 0, 1, '2020-10-10 20:42:02', '2020-10-10 13:42:02');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `avatar` varchar(256) DEFAULT NULL,
  `member_id` varchar(256) DEFAULT NULL,
  `phone` varchar(256) DEFAULT NULL,
  `provider` varchar(256) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `avatar`, `member_id`, `phone`, `provider`, `status`) VALUES
(1, 'leonferdian', 'leonardoferdiano@gmail.com', 'https://lh3.googleusercontent.com/a-/AOh14GheYHFtcCKf_ONU7I8GnDyiRMqx91nEvg7a2ilc6Q=s96-c', '106508571987407053893', NULL, 'google', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_selector` varchar(225) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(225) DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(7, '127.0.0.1', 'admin_shoesmart', '$2y$12$XdBEqxNhmQkEB8cebtz0Wu9QXV4heitmqcHJziTrMZEJmLLXu00VK', NULL, 'admin@shoesmart.com', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1602442336, 1, 'Admin', 'Shoesmart', 'Shoesmart', '031222222');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(13, 6, 1),
(14, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL,
  `product_id` varchar(250) NOT NULL,
  `customer_id` varchar(250) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_menus`
--
ALTER TABLE `admin_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer_menu`
--
ALTER TABLE `footer_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_user`
--
ALTER TABLE `log_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Server` (`ip_address`);

--
-- Indexes for table `main_menu`
--
ALTER TABLE `main_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`name`);

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `size`
--
ALTER TABLE `size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `style`
--
ALTER TABLE `style`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `top_menu`
--
ALTER TABLE `top_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `type` (`provider`),
  ADD KEY `socmed_id` (`member_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_menus`
--
ALTER TABLE `admin_menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `footer_menu`
--
ALTER TABLE `footer_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `log_user`
--
ALTER TABLE `log_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_menu`
--
ALTER TABLE `main_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `size`
--
ALTER TABLE `size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `style`
--
ALTER TABLE `style`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `top_menu`
--
ALTER TABLE `top_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
