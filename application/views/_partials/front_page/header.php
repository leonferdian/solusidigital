	<!-- HEADER -->
	<header>
		<?php $this->load->view('_partials/front_page/top_menu');?>
		<!-- header -->
		<div id="header">
			<div class="container">
				<div class="pull-left">
					<!-- Logo -->
					<div class="header-logo">
						<a class="logo" href="#">
							<img src="http://portofolio.creativedigiads.com/wp-content/uploads/2020/10/cropped-favicon-192x192.png" alt="">
						</a>
					</div>
					<!-- /Logo -->

					<!-- Search -->
					<!--<div class="header-search">
						<form>
							<input class="input search-input" type="text" placeholder="Enter your keyword">
							<select class="input search-categories" onchange="dropdown_director()">
								<option><b>Category</b></option>
								<option value="<?php //echo site_url('homeshop/category') ?>">All</option>
								<?php
									/*$categories = $this->Category_model->category();
									foreach ($categories as $row)
									{
										echo '<option value="'.site_url('homeshop/category/'.$row->name).'">'.$row->name.'</option>';
									}*/
								?>
							</select>
							<button class="search-btn"><i class="fa fa-search"></i></button>
						</form>
					</div>-->
					<!-- /Search -->
				</div>
				<div class="pull-right">
					<ul class="header-btns">
						<!-- Account -->
						<li class="header-account dropdown default-dropdown">
							<div class="dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="true">
								<div class="header-btns-icon">
									<i class="fa fa-user-o"></i>
								</div>
								<strong class="text-uppercase">My Account <i class="fa fa-caret-down"></i></strong>
							</div>
							<ul class="custom-menu">
								<?php $this->load->view('_partials/login/google_login/google_login'); ?>
								<!--<li><a href="#"><i class="fa fa-user-o"></i> My Account</a></li>-->
								<!--<li><a href="#"><i class="fa fa-heart-o"></i> My Wishlist</a></li>-->
								<!-- <li><a href="#"><i class="fa fa-exchange"></i> Compare</a></li> -->
								<!--<li><a href="#"><i class="fa fa-check"></i> Checkout</a></li>-->
								<!-- <li><a href="#"><i class="fa fa-user-plus"></i> Create An Account</a></li> -->
							</ul>
						</li>
						<!-- /Account -->

						<!-- Mobile nav toggle-->
						<li class="nav-toggle">
							<button class="nav-toggle-btn main-btn icon-btn"><i class="fa fa-bars"></i></button>
						</li>
						<!-- / Mobile nav toggle -->
					</ul>
				</div>
			</div>
			<!-- header -->
		</div>
		<!-- container -->
	</header>
    <!-- /HEADER -->