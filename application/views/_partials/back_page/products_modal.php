  <!-- The Modal -->
  <div class="modal fade" id="form_modal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Add Category</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <?php echo form_open($action_add, array('class'=>'form-horizontal form-bordered', 'id'=>'submit_form'));?>
				<div class="form-group">
					<label class="col-md-3 control-label">Name</label>
					<div class="col-md-6">
                        <div class="input-group btn-group">
                            <div class="input-group-addon"><i class="fa fa-font"></i></div>
                            <input type="text" id="name" name="name" placeholder="Name" class="form-control">
                        </div>
				    </div>
                </div>
                <div class="form-group">
					<label class="col-md-3 control-label">Price (Rp)</label>
					<div class="col-md-6">
                        <div class="input-group btn-group">
                            <div class="input-group-addon"><i>Rp.</i></div>
                            <input type="text" id="price" name="price" placeholder="0" class="form-control">
                        </div>
					</div>
                </div>
                <div class="form-group">
						<label class="col-md-3 control-label">Stock</label>
						<div class="col-md-6">
                        <div class="input-group btn-group">
                            <div class="input-group-addon"><i class="fa fa-font"></i></div>
                            <input type="text" id="stock" name="stock" placeholder="0" class="form-control">
                        </div>
					</div>
                </div>
                <div class="form-group">
					<label class="col-md-3 control-label">image</label>
					                    <div class="col-12 col-md-6<?php if(isset($errors['image'])) echo ' has-error';?>">
                                            <input type="hidden" name="size" value="1000000">
                                            <input type="file" id="image" name="image" accept="image/jpg, image/png" onchange="readURL(this);" class="form-control col-5" placeholder="">
                                                <section class="card" style="width:400px;">
                                                    <div class="card-body">
                                                        <div class="media">
                                                            <a href="#">
                                                                <?php 
                                                                    if(isset($forms['image']))
                                                                    {
                                                                        echo $forms['image'];
                                                                    }
                                                                    else
                                                                    {
                                                                        echo '<img id="image-display" src="#" alt="" />';
                                                                    }
                                                                ?>
                                                            </a>
                                                            <div class="media-body">
                                                                <h2 class="text-white display-6"></h2>
                                                                <p class="text-light"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                        </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"></label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <label>
                                <input type="checkbox" name="status" value="1" class="flat-red"<?php if(isset($action) && $action == 'edit') echo ' checked';?>>
                                Active
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-actions form-group">
                    <label class="col-md-3 control-label"></label>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-dot-circle-o"></i> Save</button>
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-ban"></i> Cancel</button>
                    </div>
                </div>
            <?php echo form_close(); ?>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
        </div>
        
      </div>
    </div>
  </div>