<?php $this->load->view('_partials/back_page/head');?>		
	<section class="body">
		<?php $this->load->view('_partials/back_page/page_header');?>
			<div class="inner-wrapper">
				<?php $this->load->view('_partials/back_page/menu');?>
				<section role="main" class="content-body">
					<?php $this->load->view('_partials/back_page/head_content');?>

					<!-- start: page -->
                        <section class="panel">
							<header class="panel-heading">
								<div class="panel-actions">
									<a href="#" class="fa fa-caret-down"></a>
									<a href="#" class="fa fa-times"></a>
								</div>
						
								<h2 class="panel-title"><?php echo isset($page_title) ? $page_title : 'Administrator' ?></h2>
							</header>
							<div class="panel-body">
                                <?php echo form_open($action, array('class'=>'form-horizontal form-bordered', 'id'=>'submit_form'));?>
									<div class="form-group">
										<label class="col-md-3 control-label">Name</label>
										<div class="col-md-6">
                                            <div class="input-group btn-group<?php if(isset($errors['name'])) echo ' has-error';?>">
                                                <div class="input-group-addon"><i class="fa fa-font"></i></div>
                                                <input type="text" id="name" name="name" placeholder="Name" class="form-control" value="<?php if(isset($forms['name'])) echo html_escape($forms['name']);?>">
                                            </div>
										</div>
                                    </div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Price (Rp)</label>
										<div class="col-md-6">
                                            <div class="input-group btn-group<?php if(isset($errors['price'])) echo ' has-error';?>">
                                                <div class="input-group-addon"><i>Rp.</i></div>
                                                <input type="text" id="price" name="price" placeholder="0" class="form-control" value="<?php if(isset($forms['price'])) echo html_escape($forms['price']);?>">
                                            </div>
										</div>
                                    </div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Stock</label>
										<div class="col-md-6">
                                            <div class="input-group btn-group<?php if(isset($errors['stock'])) echo ' has-error';?>">
                                                <div class="input-group-addon"><i class="fa fa-font"></i></div>
                                                <input type="text" id="stock" name="stock" placeholder="0" class="form-control" value="<?php if(isset($forms['stock'])) echo html_escape($forms['stock']);?>">
                                            </div>
										</div>
                                    </div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">image</label>
                                        <div class="col-12 col-md-6<?php if(isset($errors['image'])) echo ' has-error';?>">
                                            <input type="hidden" name="size" value="1000000">
                                            <input type="file" id="image" name="image" accept="image/jpg, image/png" onchange="readURL(this);" class="form-control col-5" placeholder="">
                                                <section class="card" style="width:400px;">
                                                    <div class="card-body">
                                                        <div class="media">
                                                            <a href="#">
                                                                <?php 
                                                                    if(isset($forms['image']))
                                                                    {
                                                                        echo $forms['image'];
                                                                    }
                                                                    else
                                                                    {
                                                                        echo '<img id="image-display" src="#" alt="" />';
                                                                    }
                                                                ?>
                                                            </a>
                                                            <div class="media-body">
                                                                <h2 class="text-white display-6"></h2>
                                                                <p class="text-light"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <label>
                                                    <input type="checkbox" name="status" value="1" class="flat-red"<?php if(isset($forms['status']) && $forms['status']) echo ' checked';?>>
                                                    Active
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-dot-circle-o"></i> Save</button>
                                            <a href="<?php echo site_url('administrator/products');?>" class="btn btn-danger btn-sm"><i class="fa fa-ban"></i> Cancel</a>
                                        </div>
                                    </div>
                                <?php echo form_close(); ?>
							</div>
						</section>
					<!-- end: page -->
				</section>
			</div>
		<?php $this->load->view('_partials/back_page/page_footer');?>
	</section>
<?php $this->load->view('_partials/back_page/foot');?>