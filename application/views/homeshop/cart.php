<?php $this->load->view('_partials/front_page/head');?>	
    <?php $this->load->view('_partials/front_page/header');?>	
        <?php $this->load->view('_partials/front_page/navigation');?>
        	<!-- section -->
            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div clas="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                        <th>#</th>
                                        <th>Nama Barang</th>
                                        <th>Harga</th>
                                        <th>Quantity</th>
                                        <th>Subtotal</th>
                                        <th>Action</th>
                                        </tr>
                                    <thead>
                                    <tbody>
                                            <?php
                                                foreach($cart as $row)
                                                {
                                            ?>
                                                <tr>
                                                    <td><?php echo $row->id; ?></td>
                                                    <td>
                                                        <?php 
                                                            $sql = $this->db->query("SELECT (name) FROM products WHERE code = '$row->code'");
                                                            foreach ($sql->result() as $r) {
                                                                echo $r->name;
                                                            }
                                                        ?>
                                                    </td>
                                                    <td><?php echo $row->price; ?></td>
                                                    <td><?php echo $row->quantity; ?></td>
                                                    <td><?php echo $row->subtotal; ?></td>
                                                    <td>
                                                        <?php echo '<a class="btn btn-add btn-xs btn-success fa fa-plus-square" href="'.site_url('order/cart/add?page=homeshop/cart&code='.$row->code.'').'"></a>'; ?>
                                                        <?php echo '<a class="btn btn-del btn-xs btn-warning fa fa-minus-square" href="'.site_url('order/cart/del/'.$row->id.'?page=homeshop/cart').'"></a>'; ?>
                                                        <a class="btn btn-action btn-xs btn-danger fa fa-trash" href="<?php echo site_url('order/cart/delete/'.$row->id.'/?page=homeshop/cart') ?>"></a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                    <tbody>
                                </table>
                            </div>
                            <strong>Total: Rp. <span class="text-secondary total"><?php echo $grandtotal + 19000; ?></span><strong>
                            <br>
                            <a class="btn btn-checkout btn-sm btn-warning" href="<?php echo site_url('order/cart/delete?page=homeshop/cart') ?>">Checkout</a>
                        </div>
                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </div>
            <!-- /section -->
<?php $this->load->view('_partials/front_page/foot');?>	