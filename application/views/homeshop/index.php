<?php $this->load->view('_partials/front_page/head');?>	
    <?php $this->load->view('_partials/front_page/header');?>	
        <?php $this->load->view('_partials/front_page/navigation');?>
        	<!-- section -->
            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div clas="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Stock</th>
                                        <th>Picture</th>
                                        <th>Code</th>
                                        <th>Action</th>
                                        </tr>
                                    <thead>
                                    <tbody>
                                            <?php
                                                foreach($products as $row)
                                                {
                                            ?>
                                                <tr>
                                                    <td><?php echo $row->id; ?></td>
                                                    <td><?php echo $row->name; ?></td>
                                                    <td><?php echo $row->price; ?></td>
                                                    <td><?php echo $row->stock; ?></td>
                                                    <td><?php echo $row->picture; ?></td>
                                                    <td><?php echo $row->code; ?></td>
                                                    <td><?php echo '<a class="btn btn-sm btn-success" href="'.site_url('order/cart/add?page=homeshop&code='.$row->code.'').'">Beli</a>'; ?></td>
                                                </tr>
                                                    <?php } ?>
                                    <tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </div>
            <!-- /section -->
<?php $this->load->view('_partials/front_page/foot');?>	