<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends MY_Controller 
{

	public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in())
		{
			redirect('login', 'refresh');
        }
        $this->load->model('Product_model');
	}
	
	public function index()
	{
		$data = [
			'parent_title' => '',
			'page_title' => 'Solusi Digital Admin'
        ];
        $this->_assets();
		$this->render($data);
	}

	public function products($action='', $id='')
	{
        $table = 'products';

        if ($action == 'add')
        {
            $name = isset($_POST['name']) ? trim($_POST['name']) : '';
            $forms['name'] = $name;

            $price = isset($_POST['price']) ? trim($_POST['price']) : '';
            $forms['price'] = $price;

            $stock = isset($_POST['stock']) ? trim($_POST['stock']) : '';
            $forms['stock'] = $stock;

            $image = isset($_FILES['picture']['tmp_name'])? trim($_FILES['picture']['tmp_name']) : '';
            if ($image)
            {
                $image_format = exif_imagetype($image);
                list($width, $height) = getimagesize($image);
                $new_width = 400;
                $new_height = 250;

                $theme_image_enc = base64_encode($image);
                // Resample
                $image_p = imagecreatetruecolor($new_width, $new_height);
                    
                if ($image_format == IMAGETYPE_PNG) {
                    $theme_image_little = imagecreatefrompng(base64_decode($theme_image_enc));
                } else {
                        $theme_image_little = imagecreatefromjpeg(base64_decode($theme_image_enc));
                }

                imagecopyresampled($image_p, $theme_image_little, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

                // Thanks to Michael Robinson
                // start buffering
                ob_start();
                imagejpeg($image_p);
                $contents =  ob_get_contents();
                ob_end_clean();

                $theme_image_enc_little = base64_encode($contents);

                $insert_picture = '<img id="image-display" alt="image" class="img-responsive" src="data:image;base64,'.$theme_image_enc_little.'" style="width: 400px;">';
                $forms['picture'] = $insert_picture;
            }

            $code = isset($_POST['code']) ? trim($_POST['code']) : '';
            $forms['code'] = $code;

            $status = isset($_POST['status']) ? 1 : 0;
            $forms['status'] = $status;

            $this->form_products($forms);
        }
        elseif ($action == 'edit')
        {
            $qs = $this->db->query("SELECT * FROM $table WHERE id=$id LIMIT 1");
            $row = $qs->row_array();
            if (isset($row) && $row)
            {
                return $this->form_products($row);
            }
        }
        elseif ($action == 'delete')
        {
            $sql = "DELETE FROM $table WHERE id = $id";
            $this->db->query($sql);
            redirect('administrator/'.$table, 'refresh');
        }
        
		$data = [
            'page_title' => 'Products',
            'table' => $table,
            'action_add' => 'administrator/'.$table.'/add'
        ];

        $this->_assets();
		$this->render($data);
    }

	public function listdata($table)
    {
        $response = [];
        $draw = isset($_GET['draw']) ? intval($_GET['draw']) : 1;
        $length = isset($_GET['length']) ? intval($_GET['length']) : 100000;
        $orders = isset($_GET['order']) ? $_GET['order'] : array();
        $start = isset($_GET['start']) ? intval($_GET['start']) : 0;
        $search = isset($_GET['search']['value']) ? $_GET['search']['value'] : '';

        $total = 0;
        $query = $this->db->query("SELECT COUNT(*) as total FROM $table");
        $row = $query->row();
        if (isset($row)) $total = $row->total;
        
        $total_filter = $total;
        $data = array();
        $qs = $this->db->query("SELECT * FROM $table ORDER BY id DESC LIMIT $start, $length");
        foreach($qs->result_array() as $row)
        {
            $status = $row['status'] ? '<span class="label label-success">active</span>' : '<span class="label label-default">disabled</span>';
            $btn1 = '<a class="btn btn-sm btn-info" href="'.site_url('administrator/'.$table.'/edit/'.$row['id']).'">edit</a>';
            $btn2 = '<button class="btn btn-delete btn-sm btn-danger" data-url="'.site_url('administrator/'.$table.'/delete/'.$row['id']).'">delete</button>';

            $data[] = array(
                $row['id'],
                $row['name'],
                $row['price'],
                $row['stock'],
                $row['code'],
                $status,
                $btn1.' '.$btn2
            );
        }
        $response = [
            'data' => $data,
            'draw' => $draw,
            'length' => $length,
            'recordsTotal' => $total,
            'recordsFiltered' => $total_filter
        ];

        $this->render_json($response);
	}
	
	protected function form_products($forms=array())
    {
        $errors = array();
        if (count($_POST))
        {
			$errors = array();
			
            $name = isset($_POST['name']) ? trim($_POST['name']) : '';
            $forms['name'] = $name;

            $price = isset($_POST['price']) ? trim($_POST['price']) : '';
            $forms['price'] = $price;

            $bundle_price = isset($_POST['bundle_price']) ? trim($_POST['bundle_price']) : '';
            $forms['bundle_price'] = $bundle_price;

            $stock = isset($_POST['stock']) ? trim($_POST['stock']) : '';
            $forms['stock'] = $stock;

            $query = $this->db->query("SELECT MAX(id) as total FROM products");
            foreach ($query->row() as $row) $id = $row->id + 1;

            if (str_word_count($name) > 1)
            {
                $str = $this->code_maker(2, $id);
            }
            else
            {
                $code = $this->code_maker(2, $id);
            }

            $forms['code'] = $code;

            $image = isset($_FILES['image']['tmp_name'])? trim($_FILES['image']['tmp_name']) : '';
            $image_format = exif_imagetype($image);
            list($width, $height) = getimagesize($image);
            $new_width = 400;
            $new_height = 250;

             $theme_image_enc = base64_encode($image);
            // Resample
            $image_p = imagecreatetruecolor($new_width, $new_height);
                    
            if ($image_format == IMAGETYPE_PNG) {
                    $theme_image_little = imagecreatefrompng(base64_decode($theme_image_enc));
            } else {
                 $theme_image_little = imagecreatefromjpeg(base64_decode($theme_image_enc));
            }

            imagecopyresampled($image_p, $theme_image_little, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

            // Thanks to Michael Robinson
            // start buffering
            ob_start();
            imagejpeg($image_p);
            $contents =  ob_get_contents();
            ob_end_clean();

            $theme_image_enc_little = base64_encode($contents);

            $insert_image = '<img id="image-display" alt="image" class="img-responsive" src="data:image;base64,'.$theme_image_enc_little.'" style="width: 100px;">';
            $forms['picture'] = $insert_image;

            $status = isset($_POST['status']) ? 1 : 0;
            $forms['status'] = $status;

            if (!count($errors))
            {
                $data = array(
                    'name' => $name,
                    'price' => $price,
                    'stock' => $stock,
                    'picture' => $insert_image,
                    'code' => $code,
                    'status' => $status
                );

                if (isset($forms['id']) && $forms['id'])
                {
                    $arUpdate = array();
                    $data['updated_at'] = date("Y-m-d H:i:s");
                    foreach($data as $k=>$v) $arUpdate[] = " $k='$v'";
                    $sql = "UPDATE products SET ".implode(',', $arUpdate)." WHERE id=".$forms['id']." LIMIT 1";
                }
                else
                {
                    $data['created_at'] = date("Y-m-d H:i:s");
                    $sql = "INSERT INTO products (".implode(',', array_keys($data)).") VALUES ('".implode("','", array_values($data))."')";
                }
                
                $this->db->query($sql);
                redirect('administrator/products');
            }
        }

        $data = array(
            'forms' => $forms,
            'parent_title' => 'Administrator',
            'page_title' => 'Products',
            'action' => 'administrator/products/edit/'.$forms['id'],
            'errors' => $errors
        );
        $this->_assets();
        $this->render($data, 'administrator/form_products');
    }

    private function code_maker($n, $id) 
    { 
        #$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = ''; 
      
        for ($i = 0; $i < $n; $i++) { 
            $index = rand(0, strlen($characters) - 1); 
            $randomString .= $characters[$index]; 
        } 
      
        return $randomString.$id; 
    } 

	private function _assets()
    {
        $this->add_css(site_url('assets/vendor/fontawesome-picker/css/fontawesome-iconpicker.min.css'));
        $this->add_js(site_url('assets/vendor/fontawesome-picker/js/fontawesome-iconpicker.min.js'));
        $this->add_css(site_url('assets/vendor/fontawesome-picker/css/fontawesome-iconpicker.css'));
        $this->add_js(site_url('assets/vendor/fontawesome-picker/js/fontawesome-iconpicker.js'));
        $this->add_css(site_url('assets/vendor/fontawesome-5.3.1/css/fontawesome.min.css'));
        $this->add_js(site_url('assets/vendor/fontawesome-5.3.1/js/fontawesome.min.js'));
        $this->add_css(site_url('assets/vendor/bootstrap-treeview/bootstrap-treeview.min.css'));
        $this->add_js(site_url('assets/vendor/bootstrap-treeview/bootstrap-treeview.min.js'));
        $this->add_js(site_url('assets/js/pages/admin_menu.js'));
		#vendor template
        $this->add_css(site_url('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css'));
		$this->add_css(site_url('assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css'));
		$this->add_css(site_url('assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css'));
		$this->add_css(site_url('assets/vendor/dropzone/css/basic.css'));
		$this->add_css(site_url('assets/vendor/dropzone/css/dropzone.css'));
		$this->add_css(site_url('assets/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css'));
		$this->add_css(site_url('assets/vendor/summernote/summernote.css'));
		$this->add_css(site_url('assets/vendor/summernote/summernote-bs3.css'));
		$this->add_css(site_url('assets/vendor/codemirror/lib/codemirror.css'));
		$this->add_css(site_url('assets/vendor/codemirror/theme/monokai.css'));
        $this->add_js(site_url('assets/vendor/jquery-maskedinput/jquery.maskedinput.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js'));
		$this->add_js(site_url('assets/vendor/fuelux/js/spinner.js'));
		$this->add_js(site_url('assets/vendor/dropzone/dropzone.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-markdown/js/markdown.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-markdown/js/to-markdown.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-markdown/js/bootstrap-markdown.js'));
		$this->add_js(site_url('assets/vendor/codemirror/lib/codemirror.js'));
		$this->add_js(site_url('assets/vendor/codemirror/addon/selection/active-line.js'));
		$this->add_js(site_url('assets/vendor/codemirror/addon/edit/matchbrackets.js'));
		$this->add_js(site_url('assets/vendor/codemirror/mode/javascript/javascript.js'));
		$this->add_js(site_url('assets/vendor/codemirror/mode/xml/xml.js'));
		$this->add_js(site_url('assets/vendor/codemirror/mode/htmlmixed/htmlmixed.js'));
		$this->add_js(site_url('assets/vendor/codemirror/mode/css/css.js'));
		$this->add_js(site_url('assets/vendor/summernote/summernote.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js'));
        $this->add_js(site_url('assets/vendor/ios7-switch/ios7-switch.js'));
        $this->add_css(site_url('assets/vendor/pnotify/pnotify.custom.css'));
        $this->add_js(site_url('assets/vendor/pnotify/pnotify.custom.js'));
        $this->add_js(site_url('assets/javascripts/ui-elements/examples.notifications.js'));
        $this->add_css(site_url('assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css'));
        $this->add_js(site_url('assets/vendor/jquery-autosize/jquery.autosize.js'));
        $this->add_js(site_url('assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js'));
    }
}
