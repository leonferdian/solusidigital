<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MY_Controller 
{

	public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in())
		{
			redirect('login', 'refresh');
        }
        $this->load->model('Category_model');
	}
	
	public function index()
	{
        $table = 'category';

		$data = [
            'page_title' => 'Categories',
            'table' => $table,
            'action_add' => 'categories/add'
        ];
        $this->_assets();
		$this->render($data);
	}

	public function style($action = '', $id = '')
	{
        $table = 'style';

        $op_category = '';
        $query = $this->db->query("SELECT * FROM category WHERE 1");
        foreach ($query->result() as $category)
        {
            $id = $category->id;
            $name = $category->name;
            $op_category .= '<option value="'.$id.'">'.$name.'</option>';
        }

        if ($action == 'add')
        {
            $name = isset($_POST['name']) ? trim($_POST['name']) : '';
            $forms['name'] = $name;
                
            $category_id = isset($_POST['category_id']) ? trim($_POST['category_id']) : '';
            $forms['category_id'] = $category_id;

            $status = isset($_POST['status']) ? 1 : 0;
            $forms['status'] = $status;

            $this->form_style($forms);
        }
        elseif ($action == 'edit')
        {
            $qs = $this->db->query("SELECT * FROM $table WHERE id=$id LIMIT 1");
            $row = $qs->row_array();
            if (isset($row) && $row)
            {
                return $this->form_style($row);
            }
        }
        elseif ($action == 'delete')
        {
            $sql = "DELETE FROM $table WHERE id = $id";
            $this->db->query($sql);
            redirect('categories/'.$table, 'refresh');
        }

		$data = [
            'page_title' => 'Style',
            'table' => $table,
            'op_category' => $op_category,
            'action_add' => 'categories/style/add'
        ];

        $this->_assets();
		$this->render($data);
    }
    
    protected function form_style($forms=array())
    {
        $op_category = '';
        $query = $this->db->query("SELECT * FROM style WHERE 1");
        foreach ($query->result() as $category)
        {
            $id = $category->id;
            $name = $category->name;
            $s = $id== $forms['category_id'] ? "selected" : "";
            $op_category .= '<option value="'.$id.'"'.$s.'>'.$name.'</option>';
        }

        $errors = array();
        if (count($_POST))
        {
            $errors = array();
            $name = isset($_POST['name']) ? trim($_POST['name']) : '';
            $forms['name'] = $name;

            $category_id = isset($_POST['category_id']) ? trim($_POST['category_id']) : '';
            $forms['category_id'] = $category_id;

            $status = isset($_POST['status']) ? 1 : 0;
            $forms['status'] = $status;

            if (!count($errors))
            {

                $data = array(
                    'name' => $name,
                    'category_id' => $category_id,
                    'status' => $status
                );

                if (isset($forms['id']) && $forms['id'])
                {
                    $data['updated_at'] = date("Y-m-d H:i:s");
                    $arUpdate = array();
                    foreach($data as $k=>$v) $arUpdate[] = " $k='$v'";
                    $sql = "UPDATE style SET ".implode(',', $arUpdate)." WHERE id=".$forms['id']." LIMIT 1";
                }
                else
                {
                    $data['created_at'] = date("Y-m-d H:i:s");
                    $sql = "INSERT INTO style (".implode(',', array_keys($data)).") VALUES ('".implode("','", array_values($data))."')";
                }
                
                $this->db->query($sql);
                redirect('categories/style');
            }
        }

        $data = array(
            'forms' => $forms,
            'page_title' => 'Edit Style',
            'op_category' => $op_category,
            'action' => 'categories/style/edit/'.$forms['id'],
            'errors' => $errors
        );
        $this->_assets();
        $this->render($data, 'categories/form_style');
    }

	public function listdata($table)
    {
        $response = [];
        $draw = isset($_GET['draw']) ? intval($_GET['draw']) : 1;
        $length = isset($_GET['length']) ? intval($_GET['length']) : 25;
        $orders = isset($_GET['order']) ? $_GET['order'] : array();
        $start = isset($_GET['start']) ? intval($_GET['start']) : 0;
        $search = isset($_GET['search']['value']) ? $_GET['search']['value'] : '';

        $total = 0;
        $query = $this->db->query("SELECT COUNT(*) as total FROM style");
        $row = $query->row();
        if (isset($row)) $total = $row->total;
        
        $total_filter = $total;
        $data = array();
        $qs = $this->db->query("SELECT * FROM $table ORDER BY id DESC LIMIT $start, $length");
        foreach($qs->result_array() as $row)
        {
            $status = $row['status'] ? '<span class="label label-success">active</span>' : '<span class="label label-default">disabled</span>';
            if ($table == 'category')
            {
                $btn1 = '<a class="btn btn-sm btn-edit btn-info" href="'.site_url('categories/edit/'.$row['id']).'">edit</a>';
                $btn2 = '<button class="btn btn-delete btn-sm btn-danger" data-url="'.site_url('categories/delete/'.$row['id']).'">delete</button>';
                $data[] = array(
                    $row['id'],
                    $row['name'],
                    $status,
                    $btn1.' '.$btn2
                );
            }
            else
            {
                $btn1 = '<a class="btn btn-sm btn-edit btn-info" href="'.site_url('categories/style/edit/'.$row['id']).'">edit</a>';
                $btn2 = '<button class="btn btn-delete btn-sm btn-danger" data-url="'.site_url('categories/style/delete/'.$row['id']).'">delete</button>';
                $get_category = $this->Category_model->category('WHERE id ='.$row['category_id']);
                foreach ($get_category as $cat) $category = $cat->name;
                $data[] = array(
                    $row['id'],
                    $row['name'],
                    $category,
                    $status,
                    $btn1.' '.$btn2
                );
            }
            
        }
        $response = [
            'data' => $data,
            'draw' => $draw,
            'length' => $length,
            'recordsTotal' => $total,
            'recordsFiltered' => $total_filter
        ];

        $this->render_json($response);
	}

	public function add()
    {
        $name = isset($_POST['name']) ? trim($_POST['name']) : '';
        $forms['name'] = $name;

        $status = isset($_POST['status']) ? 1 : 0;
        $forms['status'] = $status;

        $this->form_category($forms);
	}
	
	public function edit($id)
    {
        if (!$id)
        {
            $this->session->set_flashdata('error', 'Invalid edit user url');
            redirect('administrator/products');
        }

        $qs = $this->db->query("SELECT * FROM category WHERE id=$id LIMIT 1");
        $row = $qs->row_array();
        if (isset($row) && $row)
        {
            return $this->form_category($row);
        }
    }

	public function delete($id)
    {
        if (!$id)
        {
            $this->session->set_flashdata('error', 'Invalid delete url!');
            redirect('categories');
        }

        if ($id)
        {
            $sql = "DELETE FROM category WHERE id = $id";
            $this->db->query($sql);
            redirect('categories', 'refresh');
        }
    }

    protected function form_category($forms=array())
    {
        if (isset($forms['id']) && $forms['id'])
        {
            $action = 'categories/edit/'.$forms['id'];
        }
        else
        {
            $action = 'categories/add';
        }

        $errors = array();
        if (count($_POST))
        {
            $errors = array();
            $name = isset($_POST['name']) ? trim($_POST['name']) : '';
            $forms['name'] = $name;

            $status = isset($_POST['status']) ? 1 : 0;
            $forms['status'] = $status;

            if (!count($errors))
            {

                $data = array(
                    'name' => $name,
                    'status' => $status
                );

                if (isset($forms['id']) && $forms['id'])
                {
                    $data['updated_at'] = date("Y-m-d H:i:s");
                    $arUpdate = array();
                    foreach($data as $k=>$v) $arUpdate[] = " $k='$v'";
                    $sql = "UPDATE category SET ".implode(',', $arUpdate)." WHERE id=".$forms['id']." LIMIT 1";
                }
                else
                {
                    $data['created_at'] = date("Y-m-d H:i:s");
                    $sql = "INSERT INTO category (".implode(',', array_keys($data)).") VALUES ('".implode("','", array_values($data))."')";
                }
                
                $this->db->query($sql);
                redirect('categories');
            }
        }

        $data = array(
            'forms' => $forms,
            'page_title' => 'Categories',
            'action' => $action,
            'errors' => $errors
        );
        $this->_assets();
        $this->render($data, 'categories/form_category');
    }

	private function _assets()
    {
        $this->add_css(site_url('assets/vendor/fontawesome-picker/css/fontawesome-iconpicker.min.css'));
        $this->add_js(site_url('assets/vendor/fontawesome-picker/js/fontawesome-iconpicker.min.js'));
        $this->add_css(site_url('assets/vendor/fontawesome-picker/css/fontawesome-iconpicker.css'));
        $this->add_js(site_url('assets/vendor/fontawesome-picker/js/fontawesome-iconpicker.js'));
        $this->add_css(site_url('assets/vendor/fontawesome-5.3.1/css/fontawesome.min.css'));
        $this->add_js(site_url('assets/vendor/fontawesome-5.3.1/js/fontawesome.min.js'));
        $this->add_css(site_url('assets/vendor/bootstrap-treeview/bootstrap-treeview.min.css'));
        $this->add_js(site_url('assets/vendor/bootstrap-treeview/bootstrap-treeview.min.js'));
        $this->add_js(site_url('assets/js/pages/admin_menu.js'));
		#vendor template
        $this->add_css(site_url('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css'));
		$this->add_css(site_url('assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css'));
		$this->add_css(site_url('assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css'));
		$this->add_css(site_url('assets/vendor/dropzone/css/basic.css'));
		$this->add_css(site_url('assets/vendor/dropzone/css/dropzone.css'));
		$this->add_css(site_url('assets/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css'));
		$this->add_css(site_url('assets/vendor/summernote/summernote.css'));
		$this->add_css(site_url('assets/vendor/summernote/summernote-bs3.css'));
		$this->add_css(site_url('assets/vendor/codemirror/lib/codemirror.css'));
        $this->add_css(site_url('assets/vendor/codemirror/theme/monokai.css'));
        $this->add_js(site_url('assets/vendor/jquery-maskedinput/jquery.maskedinput.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js'));
		$this->add_js(site_url('assets/vendor/fuelux/js/spinner.js'));
		$this->add_js(site_url('assets/vendor/dropzone/dropzone.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-markdown/js/markdown.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-markdown/js/to-markdown.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-markdown/js/bootstrap-markdown.js'));
		$this->add_js(site_url('assets/vendor/codemirror/lib/codemirror.js'));
		$this->add_js(site_url('assets/vendor/codemirror/addon/selection/active-line.js'));
		$this->add_js(site_url('assets/vendor/codemirror/addon/edit/matchbrackets.js'));
		$this->add_js(site_url('assets/vendor/codemirror/mode/javascript/javascript.js'));
		$this->add_js(site_url('assets/vendor/codemirror/mode/xml/xml.js'));
		$this->add_js(site_url('assets/vendor/codemirror/mode/htmlmixed/htmlmixed.js'));
		$this->add_js(site_url('assets/vendor/codemirror/mode/css/css.js'));
		$this->add_js(site_url('assets/vendor/summernote/summernote.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js'));
        $this->add_js(site_url('assets/vendor/ios7-switch/ios7-switch.js'));
        $this->add_css(site_url('assets/vendor/pnotify/pnotify.custom.css'));
        $this->add_js(site_url('assets/vendor/pnotify/pnotify.custom.js'));
        $this->add_js(site_url('assets/javascripts/ui-elements/examples.notifications.js'));
    }
}
