<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homeshop extends MY_Controller 
{

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Menu_model');
        $this->load->model('Category_model');
        $this->load->model('Product_model');
	}
	
	public function index()
	{
        $start = 0;
        $length = 10;
        $query = $this->db->query("SELECT * FROM products WHERE status = 1 AND stock > 0 LIMIT $start, $length");
        $products = $query->result();

        $qs = $this->db->query("SELECT COUNT(*) as total_barang FROM cart");
        $qt = $qs->result();
        foreach ($qt as $row) $q = $row->total_barang;

        $qs2 = $this->db->query("SELECT SUM(`subtotal`) as grandtotal FROM cart");
        $sb = $qs2->result();
        foreach ($sb as $row) 
        {
            $grandtotal = $row->grandtotal;
        }

		$data = [
            'page_title' => 'Solusi Digital Market',
            'products' => $products,
            'q' => $q,
            'grandtotal' => $grandtotal
        ];
        
		$this->render($data);
    }

    public function cart($action = '')
	{
        $cart = $this->Product_model->cart();

        $qs = $this->db->query("SELECT COUNT(*) as total_barang FROM cart");
        $qt = $qs->result();
        foreach ($qt as $row) $q = $row->total_barang;

        $qs2 = $this->db->query("SELECT SUM(`subtotal`) as grandtotal FROM cart");
        $sb = $qs2->result();
        foreach ($sb as $row) 
        {
            $grandtotal = $row->grandtotal;
        }

		$data = [
            'page_title' => 'Cart',
            'cart' => $cart,
            'q' => $q,
            'grandtotal' => $grandtotal
        ];
        
		$this->render($data);
    }
    
    public function category($category='', $style='')
	{
        $cat = $this->Category_model->category("WHERE name= '$category'");
        foreach ($cat as $row) $category_id = $row->id;

        if (!empty($category))
        {
            $where = [];
            $where[] = "category = '$category_id'";
            if (!empty($style))
            {
                $style = $this->Category_model->style("WHERE name= '$style' AND category_id = $category_id");
                foreach ($style as $row) $style_id = $row->id;
                $where[] = "style = '$style_id'";
            }
            
            $wherestr = 'WHERE '.implode(" AND ", $where);
            $products = $this->Product_model->product($wherestr);
            foreach ($products as $row)
            {
                $name = $row->name;
                $picture = $row->picture;
                $single_price = $row->single_price;
                $promo_stock = $row->promo_stock;
                $rate = $this->Product_model->get_rate($row->product_id);
            }
        }

		$data = [
            'page_title' => 'Category'
        ];
        
		$this->render($data);
    }

	private function _assets()
    {
        $this->add_css(site_url('assets/vendor/fontawesome-picker/css/fontawesome-iconpicker.min.css'));
        $this->add_js(site_url('assets/vendor/fontawesome-picker/js/fontawesome-iconpicker.min.js'));
        $this->add_css(site_url('assets/vendor/fontawesome-picker/css/fontawesome-iconpicker.css'));
        $this->add_js(site_url('assets/vendor/fontawesome-picker/js/fontawesome-iconpicker.js'));
        $this->add_css(site_url('assets/vendor/fontawesome-5.3.1/css/fontawesome.min.css'));
        $this->add_js(site_url('assets/vendor/fontawesome-5.3.1/js/fontawesome.min.js'));
        $this->add_css(site_url('assets/vendor/bootstrap-treeview/bootstrap-treeview.min.css'));
        $this->add_js(site_url('assets/vendor/bootstrap-treeview/bootstrap-treeview.min.js'));
        $this->add_js(site_url('assets/js/pages/admin_menu.js'));
		#vendor template
        $this->add_css(site_url('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css'));
		$this->add_css(site_url('assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css'));
		$this->add_css(site_url('assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css'));
		$this->add_css(site_url('assets/vendor/dropzone/css/basic.css'));
		$this->add_css(site_url('assets/vendor/dropzone/css/dropzone.css'));
		$this->add_css(site_url('assets/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css'));
		$this->add_css(site_url('assets/vendor/summernote/summernote.css'));
		$this->add_css(site_url('assets/vendor/summernote/summernote-bs3.css'));
		$this->add_css(site_url('assets/vendor/codemirror/lib/codemirror.css'));
		$this->add_css(site_url('assets/vendor/codemirror/theme/monokai.css'));
        $this->add_js(site_url('assets/vendor/jquery-maskedinput/jquery.maskedinput.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js'));
		$this->add_js(site_url('assets/vendor/fuelux/js/spinner.js'));
		$this->add_js(site_url('assets/vendor/dropzone/dropzone.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-markdown/js/markdown.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-markdown/js/to-markdown.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-markdown/js/bootstrap-markdown.js'));
		$this->add_js(site_url('assets/vendor/codemirror/lib/codemirror.js'));
		$this->add_js(site_url('assets/vendor/codemirror/addon/selection/active-line.js'));
		$this->add_js(site_url('assets/vendor/codemirror/addon/edit/matchbrackets.js'));
		$this->add_js(site_url('assets/vendor/codemirror/mode/javascript/javascript.js'));
		$this->add_js(site_url('assets/vendor/codemirror/mode/xml/xml.js'));
		$this->add_js(site_url('assets/vendor/codemirror/mode/htmlmixed/htmlmixed.js'));
		$this->add_js(site_url('assets/vendor/codemirror/mode/css/css.js'));
		$this->add_js(site_url('assets/vendor/summernote/summernote.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js'));
		$this->add_js(site_url('assets/vendor/ios7-switch/ios7-switch.js'));
    }
}
