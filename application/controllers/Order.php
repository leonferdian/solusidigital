<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller 
{

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Product_model');
	}
	
	public function index()
	{
        $table = 'cart';
		$data = [
            'page_title' => 'List Order',
            'table' => $table
        ];
        $this->_assets();
		$this->render($data);
	}

	public function cart($action='', $id='')
	{
        $table = 'cart';
        $page = isset($_GET['page']) ? trim($_GET['page']) : '';
        $code = isset($_GET['code']) ? trim($_GET['code']) : '';
        $query = $this->db->query("SELECT quantity, subtotal FROM cart WHERE code = '$code'");
        foreach ($query->result() as $row) 
        {
            $quantity = $row->quantity;
            $l_subtotal = $row->subtotal;
        }

        $qs = $this->db->query("SELECT price FROM products WHERE code = '$code'");
        foreach ($qs->result() as $row) $price = $row->price;

        $subtotal = $l_subtotal + $price;
        
        $add = $quantity + 1;
        $del = $quantity - 1;

        if ($action == 'add')
        {
            $sql = $this->db->query("SELECT * FROM cart WHERE code = '$code'");
            $exist = $sql->result();
            

            $forms = array(
                'code' => $code,
                'price' => $price,
                'quantity' => 1,
                'subtotal' => $price
            );

            if ($exist)
            {
                $sql = "UPDATE cart SET quantity = $add, subtotal = $subtotal WHERE code = '$code'";
            }
            else
            {
                $forms['created_at'] = date("Y-m-d H:i:s");
                $sql = "INSERT INTO cart (".implode(',', array_keys($forms)).") VALUES ('".implode("','", array_values($forms))."')";
            }
            $this->db->query($sql);
            redirect($page);
        }
        if ($action == 'del')
        {
            $sql = "UPDATE cart SET quantity = $del WHERE id = '$id'";
            $this->db->query($sql);
            redirect($page);
        }
        elseif ($action == 'edit')
        {
            $qs = $this->db->query("SELECT * FROM $table WHERE id=$id LIMIT 1");
            $row = $qs->row_array();
            if (isset($row) && $row)
            {
                return $this->form_cart($row);
            }
        }
        elseif ($action == 'delete')
        {
            if ($id)
            {
                $sql = "DELETE FROM $table WHERE id = $id";
                $this->db->query($sql);
            }
            else
            {
                $sql = "DELETE FROM $table";
                $this->db->query($sql);
            }
            redirect($page);
        }
        
		$data = [
            'page_title' => 'Cart',
            'table' => $table
        ];

        $this->_assets();
		$this->render($data);
    }

	public function listdata($table)
    {
        $response = [];
        $draw = isset($_GET['draw']) ? intval($_GET['draw']) : 1;
        $length = isset($_GET['length']) ? intval($_GET['length']) : 100000;
        $orders = isset($_GET['order']) ? $_GET['order'] : array();
        $start = isset($_GET['start']) ? intval($_GET['start']) : 0;
        $search = isset($_GET['search']['value']) ? $_GET['search']['value'] : '';

        $total = 0;
        $query = $this->db->query("SELECT COUNT(*) as total FROM $table");
        $row = $query->row();
        if (isset($row)) $total = $row->total;
        
        $total_filter = $total;
        $data = array();
        $qs = $this->db->query("SELECT * FROM $table ORDER BY id DESC LIMIT $start, $length");
        foreach($qs->result_array() as $row)
        {
            $btn1 = '<a class="btn btn-sm btn-info" href="'.site_url('order/'.$table.'/edit/'.$row['id']).'">edit</a>';
            $btn2 = '<button class="btn btn-delete btn-sm btn-danger" data-url="'.site_url('order/'.$table.'/delete/'.$row['id']).'">delete</button>';
            $where = 'WHERE code = '.$row['code'];
            $product = $this->Product_model->product($where);
            foreach ($product as $product) $product_name = $product->name;
            if ($table == 'cart')
            {
                $data[] = array(
                    $row['id'],
                    $row['code'],
                    $product_name,
                    $row['price'],
                    $btn1.' '.$btn2
                );
            }
            else
            {

            }
        }
        $response = [
            'data' => $data,
            'draw' => $draw,
            'length' => $length,
            'recordsTotal' => $total,
            'recordsFiltered' => $total_filter
        ];

        $this->render_json($response);
	}
	
    protected function form_cart($forms=array())
    {
        $op_status = '<option value="1">Active</option><option value="0">Passed</option>';

        $errors = array();
        if (count($_POST))
        {
			$errors = array();
			
            $code = isset($_POST['code']) ? trim($_POST['code']) : '';
            $forms['code'] = $code;

            $quantipricety = isset($_POST['price']) ? trim($_POST['price']) : 0;
            $forms['price'] = $price;

            if (!count($errors))
            {
                $data = array(
                    'code' => $code,
                    'price' => $price
                );

                if (isset($forms['id']) && $forms['id'])
                {
                    $arUpdate = array();
                    $data['updated_at'] = date("Y-m-d H:i:s");
                    foreach($data as $k=>$v) $arUpdate[] = " $k='$v'";
                    $sql = "UPDATE cart SET ".implode(',', $arUpdate)." WHERE id=".$forms['id']." LIMIT 1";
                }
                else
                {
                    $data['created_at'] = date("Y-m-d H:i:s");
                    $sql = "INSERT INTO cart (".implode(',', array_keys($data)).") VALUES ('".implode("','", array_values($data))."')";
                    redirect('homeshop');
                }
                
                $this->db->query($sql);
            }
        }

        $data = array(
            'forms' => $forms,
            'parent_title' => 'Order',
            'page_title' => 'Cart',
            'action' => 'order/cart/edit/'.$forms['id'],
            'op_category' => $op_status,
            'errors' => $errors
        );
        $this->_assets();
        $this->render($data, 'order/form_cart');
    }

    private function code_maker($n) { 
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
        $randomString = ''; 
      
        for ($i = 0; $i < $n; $i++) { 
            $index = rand(0, strlen($characters) - 1); 
            $randomString .= $characters[$index]; 
        } 
      
        return $randomString; 
    } 

	private function _assets()
    {
        $this->add_css(site_url('assets/vendor/fontawesome-picker/css/fontawesome-iconpicker.min.css'));
        $this->add_js(site_url('assets/vendor/fontawesome-picker/js/fontawesome-iconpicker.min.js'));
        $this->add_css(site_url('assets/vendor/fontawesome-picker/css/fontawesome-iconpicker.css'));
        $this->add_js(site_url('assets/vendor/fontawesome-picker/js/fontawesome-iconpicker.js'));
        $this->add_css(site_url('assets/vendor/fontawesome-5.3.1/css/fontawesome.min.css'));
        $this->add_js(site_url('assets/vendor/fontawesome-5.3.1/js/fontawesome.min.js'));
        $this->add_css(site_url('assets/vendor/bootstrap-treeview/bootstrap-treeview.min.css'));
        $this->add_js(site_url('assets/vendor/bootstrap-treeview/bootstrap-treeview.min.js'));
        $this->add_js(site_url('assets/js/pages/admin_menu.js'));
		#vendor template
        $this->add_css(site_url('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css'));
		$this->add_css(site_url('assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css'));
		$this->add_css(site_url('assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css'));
		$this->add_css(site_url('assets/vendor/dropzone/css/basic.css'));
		$this->add_css(site_url('assets/vendor/dropzone/css/dropzone.css'));
		$this->add_css(site_url('assets/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css'));
		$this->add_css(site_url('assets/vendor/summernote/summernote.css'));
		$this->add_css(site_url('assets/vendor/summernote/summernote-bs3.css'));
		$this->add_css(site_url('assets/vendor/codemirror/lib/codemirror.css'));
		$this->add_css(site_url('assets/vendor/codemirror/theme/monokai.css'));
        $this->add_js(site_url('assets/vendor/jquery-maskedinput/jquery.maskedinput.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js'));
		$this->add_js(site_url('assets/vendor/fuelux/js/spinner.js'));
		$this->add_js(site_url('assets/vendor/dropzone/dropzone.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-markdown/js/markdown.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-markdown/js/to-markdown.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-markdown/js/bootstrap-markdown.js'));
		$this->add_js(site_url('assets/vendor/codemirror/lib/codemirror.js'));
		$this->add_js(site_url('assets/vendor/codemirror/addon/selection/active-line.js'));
		$this->add_js(site_url('assets/vendor/codemirror/addon/edit/matchbrackets.js'));
		$this->add_js(site_url('assets/vendor/codemirror/mode/javascript/javascript.js'));
		$this->add_js(site_url('assets/vendor/codemirror/mode/xml/xml.js'));
		$this->add_js(site_url('assets/vendor/codemirror/mode/htmlmixed/htmlmixed.js'));
		$this->add_js(site_url('assets/vendor/codemirror/mode/css/css.js'));
		$this->add_js(site_url('assets/vendor/summernote/summernote.js'));
		$this->add_js(site_url('assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js'));
        $this->add_js(site_url('assets/vendor/ios7-switch/ios7-switch.js'));
        $this->add_css(site_url('assets/vendor/pnotify/pnotify.custom.css'));
        $this->add_js(site_url('assets/vendor/pnotify/pnotify.custom.js'));
        $this->add_js(site_url('assets/javascripts/ui-elements/examples.notifications.js'));
        $this->add_css(site_url('assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css'));
        $this->add_js(site_url('assets/vendor/jquery-autosize/jquery.autosize.js'));
        $this->add_js(site_url('assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js'));
    }
}
