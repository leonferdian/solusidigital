<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {

    public function product($where = '', $limit = '')
    {
        $query = $this->db->query("SELECT * FROM products $where ORDER BY id $limit");
        $row = $query->result();
		return $row;
    }

    public function cart()
    {
        $query = $this->db->query("SELECT * FROM cart ORDER BY id");
        $row = $query->result();
		return $row;
    }

    public function quantity($code = '')
    {
        $query = $this->db->query("SELECT COUNT(*) as quantity FROM cart WHERE code = $code");
        $row = $query->result();
		return $row;
    }

    public function get_rate($product_id = '')
    {
        $query = $this->db->query("SELECT * FROM rate WHERE product_id = $product_id LIMIT 1");
        $row = $query->result();
		return $row;
    }
}