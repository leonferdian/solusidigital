<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {

    public function category($where = '')
    {
        $query = $this->db->query("SELECT * FROM category $where ORDER BY id");
        $row = $query->result();
		return $row;
    }

    public function style($where = '')
    {
        $query = $this->db->query("SELECT * FROM style $where ORDER BY id");
        $row = $query->result();
		return $row;
    }
}